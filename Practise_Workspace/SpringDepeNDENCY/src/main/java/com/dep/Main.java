package com.dep;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
		public static void main(String[] args) {
			/**Student student=new Student();
			student.setStudentName("Ramu");
			student.display();**/ //no need to create objects for another class when using spring
			ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
			Student stu=context.getBean("student",Student.class);
			stu.display();
		}

}









//Spring contains IOC which creates objects and also we can use setter or construction injection.
package com.dep;

public class Student {
	private String studentName;

	public Student() {
		super();
	}
	
	public Student(String studentName) {
		super();
		this.studentName = studentName;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public void display()
	{
		System.out.println("Student name is" +studentName);
	}

}

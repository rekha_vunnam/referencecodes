package com.annot;

public class Student {
	private String name;
	private int num;
	private String hobbies;
	public Student() {
		super();
	}
	public Student(String name, int num, String hobbies) {
		super();
		this.name = name;
		this.num = num;
		this.hobbies = hobbies;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getHobbies() {
		return hobbies;
	}
	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

}

package com.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Human {
	 @Autowired
	 @Qualifier("Octopusheart")
	private Heart heart;

	public Human() {
		super();
	}

	public Human(Heart heart) {
		super();
		this.heart = heart;
	}

	public Heart getHeart() {
		return heart;
	}
   
	public void setHeart(Heart heart) {
		this.heart = heart;
	}
    public void startPumping()
    {
    	if(heart !=null)
    	{
    	   heart.pump();
    	   System.out.println("The name of heart is " +heart.getName()+" and number of hearts are " +heart.getNumber());
    	}
    	else
    	{
    		System.out.println("you are dead");
    	}
    }
}

package com.annotation;

public class Heart {
	private String name;
	private int number;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public void pump()
	{
		System.out.println("pumping");
		System.out.println("alive");
	}

}

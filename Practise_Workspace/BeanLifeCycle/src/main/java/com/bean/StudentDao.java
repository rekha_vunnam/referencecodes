package com.bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class StudentDao {
	private String driver;
	private String url;
	private String userName;
	private String password;
    Connection con;

	public StudentDao() {
		super();
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		System.out.println("setting driver");
		this.driver = driver;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		System.out.println("setting url");
		this.url = url;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		System.out.println("setting password");
		this.password = password;
	}
	// @PostConstruct
	public void init() throws ClassNotFoundException, SQLException
	{
		 System.out.println("inside the custom init method");
		createStudentStatement();
	}
	public void createStudentStatement() throws ClassNotFoundException, SQLException {
		// load driver
    	 System.out.println("Creating connection");
		Class.forName(driver);

		// get a connection
	   con= DriverManager.getConnection(url, userName, password);

	}
     
	public void selectAllRows() throws ClassNotFoundException, SQLException {
		//createStudentStatement();-->each time we need not to call this method ,spring only will do it.
		// execute query
		System.out.println("retrieving all");
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from training.emp1hiber");
		while (rs.next()) {
			int empNumber = rs.getInt(1);
			String eName = rs.getString(2);
			float salary = rs.getFloat(3);
			System.out.println(empNumber + "" + eName + "" + salary);

		}
	}
 
	public void deleteStudentRecord(int empNumber) throws ClassNotFoundException, SQLException {
		
		// execute query
		Statement stmt = con.createStatement();
		stmt.executeUpdate("delete from training.emp1hiber where empnumber= " + empNumber);
		System.out.println("delete student with id " + empNumber);
		
	}
	
	public void closeConnection() throws SQLException
	{
		
		//closing connection
		
		con.close();
	}
	//@PreDestroy //before we are removing we need to close all methods
	public void destroy() throws SQLException
	{
		System.out.println("closing connection");
		closeConnection();
	}

}

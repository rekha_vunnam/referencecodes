package com.bean;

import java.sql.SQLException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		/*
		 * StudentDao dao=new StudentDao(); dao.selectAllRows();
		 * dao.deleteStudentRecord(2);
		 */

		// --Avoiding above lines we are continuing by writing below using spring--.
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		StudentDao dao = context.getBean("dao", StudentDao.class);
		System.out.println(dao);
		dao.selectAllRows();
		((ClassPathXmlApplicationContext)context).close();//ApplicationContext doesnot contain close method,so we have to downcast it

	}

}

package com.scope;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("student")
@Scope("singleton")
public class Student {
	@Autowired     //here we are calling school class and creating object for that thatsy that constructor also get displayed in output
	 private School school;

	public Student() {
		System.out.println("Student object created");
	}
	@Lookup
	School createScoolObj()
	{
		return null;
	}

	public School getSchool() {
		School school=createScoolObj();
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}
	
}


//when we mark bean as singleton that clss is calld without calling that bean (Ex:context.getBean()->without using this) th constructor get calld.
//Here we calld another class and annotated with autowired that's y that particular class constructor also called even though that class is marked as prototype.
package com.scope;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype"/*,proxyMode= ScopedProxyMode.TARGET_CLASS*/) //create proxy(another Scool class) class
public class School {
	private String name;
	

	public School() {
		System.out.println("School object created");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

//ScopedProxyMode.TARGET_CLASS->creates proxy class using CG(core generation library) library.

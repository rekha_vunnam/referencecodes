package com.scope;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) {
		
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
		//for singleton explanation
		
		/*SingletonDemo obj1=context.getBean("singletonDemo",SingletonDemo.class);
		SingletonDemo obj2=context.getBean("singletonDemo",SingletonDemo.class);
		System.out.println(obj1+ " " +obj2);*/
		
		//for prototype explanation
		
		/*PrototypeDemo obj1=context.getBean("prototypeDemo",PrototypeDemo.class);
		PrototypeDemo obj2=context.getBean("prototypeDemo",PrototypeDemo.class);
		System.out.println(obj1+ " " +obj2);
		if(obj1==obj2)
		{
			System.out.println("same instance");
		}
		else
		{
			System.out.println("not same");
		}*/
		
		//Explaining difference between sinngleton and prototype scopes
		/*Student studentobj=context.getBean("student",Student.class);
		//Student studentobj2=context.getBean("student",Student.class);
		//System.out.println(studentobj +" " + studentobj2);
		System.out.println(studentobj.getSchool()); 
		System.out.println(studentobj.getSchool());*/
		
		//explaining singleton using two different application context
		/*ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
		SingletonDemo obj1=context.getBean("singletonDemo",SingletonDemo.class);
		System.out.println(obj1);
		
		ApplicationContext context2=new ClassPathXmlApplicationContext("beans.xml");
		SingletonDemo obj2=context2.getBean("singletonDemo",SingletonDemo.class);
		System.out.println(obj2);*/
		
	}

}


//both obj1 and obj2 refer to same,that means scope is singleton

//System.out.println(studentobj.getSchool()); 
//System.out.println(studentobj.getSchool()); for this two methods we should get two different references
//but we are getting same instance bcse it is placed inside singleton class annotated with singleton.

//whener we use proxy in a class marked scope with prototype we will get different instances for above two Sysouts.
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.error{
     color:red;
     position:fixed;
     text-align:left;
     margin-left:30px;
    }
</style>
<script>
function validateUserInfo()
{
	var Name=document.getElementById("yn").value;
	if(Name.length<2)
		{
		alert("Name should be atleat 2 characters");
		return false;
		}
	else 
	{
		return true;
	}
	}
	
</script>
</head>
<body>
	<h1 align="center">Calculator</h1>
	<form:form action="process-homepage" method="post"
		modelAttribute="userInfo" onsubmit="return validateUserInfo()">
		<div align="center">
			<p>
				<label for="yn">YourName : </label>
				<form:input id="yn" path="userName" />
				<form:errors path="userName" class="error"/>
			</p>
			<p>
				<label for="cn">CrushName : </label>
				<form:input id="cn" path="crushName" />
			</p>
			<p>
				<label>Password : </label>
				<form:password path="password" />
			</p>
			<p>
				<label>Country : </label>
				<form:select path="country">
					<form:option value="india">India</form:option>
					<form:option value="italy">Italy</form:option>>
					<form:option value="Switzerland">SwitzerLand</form:option>>
				</form:select>
			</p>
			<p>
				<label>Hobby : </label>
				<form:checkbox path="hobby" value="cricket" />
				Cricket
				<form:checkbox path="hobby" value="reading" />
				Reading
				<form:checkbox path="hobby" value="travel" />
				Travel
				<form:checkbox path="hobby" value="programming" />
				Programming
			</p>
			<p>
				<label >Gender : </label>
				<form:radiobutton path="gender" value="male" />
				Male
				<form:radiobutton path="gender" value="female" />
				Female
				<p>
				<form:checkbox path="validate" id="check"/>
				<label>I am accepting this</label>
				<form:errors path="validate"/>

			</p>
			<input type="submit" value="calculate">
		</div>
	</form:form>
</body>
</html>
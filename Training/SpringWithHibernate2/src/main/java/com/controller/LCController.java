package com.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.User;

@Controller
public class LCController {
     @RequestMapping("/")
	public String showHomePage(@ModelAttribute("userInfo")User user)//we can also use here if we have any default values in pojo,to bind
	{
		return "home-page";
	}
    /* @RequestMapping("process-homepage")
     public String showResultPage(@RequestParam("userName") String userName1,@RequestParam("crushName") String crushName1,Model model)
     {
    	 model.addAttribute("userName", userName1);//inside "" we can give anyname
    	 model.addAttribute("crushName", crushName1);
    	 return "resultPage";
     }*/
     //Without using requestparam,we are binding with Classname.
     /*@RequestMapping("process-homepage")
     public String showResultPage(User user,Model model)
     {
    	 model.addAttribute("userInfo", user);//inside "" we can give anyname
    
    	 return "resultPage";
     }*/
     //Using model-Attribute
     @RequestMapping("/process-homepage")
     public String showResultPage(@Valid @ModelAttribute("userInfo")User user,BindingResult result)
     {
         if(result.hasErrors())
         {
        	/*List<ObjectError>allErrors =result.getAllErrors();
        	for(ObjectError temp : allErrors)
        	{
        		System.out.println(temp);
        	}*/
        	 return "home-page";//if error occurs stays in home-page
         }
    	 return "resultPage";
     }

}

/*Here inside @RequestName we gave name as userName that should be matched with the name in jsp page(home-page.jsp(name=userName),here we are using @Requestparam to bind the result from url page.For example when we click on calculate we are getting username and crush name in url instead of showing in url,if we want the result to be displayed in result page we will bind the result using requestparam.
 * There is one disadvantage with requestparam if we have so many fields we cant write 100 lines with model.addattribute,so we create a pojo class and spring automatically cretaes object and binds result.*/
 //bindingresilt stores errors
package com.dao;

import java.util.List;

import com.model.Employee;

public interface EmployeeDao {
	public abstract Employee createEmployee(Employee employee);

	public abstract Employee readByEmployeeId(int empid);

	public abstract Employee readByEmployeeName(String empName);

	public abstract Employee updateEmployee(Employee employee);

	public abstract int deleteByEmployeeId(int empId);
	
	public abstract int deleteByEmployeeName(int empNAme);

	public abstract List<Employee> listAllEmployee();

}

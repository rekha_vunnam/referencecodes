package com.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class OneToMany {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("com/config/hibernate.cfg.xml")
				.build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		Transaction transaction = null;
		Teacher teacher = new Teacher();
		teacher.setTeacherId(100);
		teacher.setTeacherName("First Teacher");
		Student student1 = new Student();
		student1.setStuId(1);
		student1.setSname("One");
		student1.setTeacher(teacher);
		Student student2 = new Student();
		student2.setStuId(2);
		student2.setSname("two");
		student2.setTeacher(teacher);
		List<Student> students = new ArrayList();
		students.add(student1);
		students.add(student2);
		teacher.setStudents(students);
		transaction = session.beginTransaction();
		// session.save(student1) ;
		// session.save(student2);
		session.save(teacher);
		transaction.commit();
		System.out.println("End of One to many");
		factory.close();

	}
}

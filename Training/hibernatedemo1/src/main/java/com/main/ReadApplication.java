package com.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;

public class ReadApplication {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("com/config/hibernate.cfg.xml")
		.build();
	Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
	SessionFactory factory = meta.getSessionFactoryBuilder().build();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction(); // commit & rollback
	Employee obj = session.get(Employee.class, 1);// here 1 should be a primaryKey.
	if(obj!=null)
	{
		System.out.println(obj.getEmpNo());
		System.out.println(obj.getEmpName());
		System.out.println(obj.getSalary());
		obj.setEmpName("update name");//we can update only existing data
		session.update(obj); 
		//session.delete(obj);//if we want to delete any record
		transaction.commit();//after updating we have to commit,otherwise it wont be committed
	}
	
	else
	{
		System.out.println("employee not found");
	}
	System.out.println("successfully saved");
	}	
}

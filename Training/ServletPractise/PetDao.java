package com.dao;



import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.model.Pet;

public interface PetDao extends PagingAndSortingRepository<Pet,Integer>,CrudRepository<Pet,Integer>{
	List<Pet> findAllById(int ownerId);
	List<Pet> findAllById(int id,Pageable pageable);
	public void save();
	List<Pet> getallPets();
	List<Pet> findAllById(Integer ownerId);
	Optional<Pet> findById(Integer petId);
	List<Pet> findAll();
	Page<Pet> findAll(Pageable pageable);

}

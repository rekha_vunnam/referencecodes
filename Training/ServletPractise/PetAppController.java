package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dao.PetDao;
import com.dao.UserDao;
import com.model.Pet;
import com.model.User;

@Controller
@RequestMapping(value="/pet")
public class PetAppController {
	@Autowired
	PetDao petDao;
	UserDao userDao;
	DataSource datasource;
	public ModelAndView Login()
	{
		return new ModelAndView("login","command",new User());
	}
    //@RequestMapping(value="/login",method=RequestMethod.POST)
   /* public ModelAndView authenticateUser(@ModelAttribute("user")User user,ModelMap model)
    {
    	User user1=userDao.findByUserNameAndPassword(user.getUserName(), user.getPassword());
    	if(user1==null) {
    		System.out.println("Invalid user");
    		return new ModelAndView("login","msg","Invalid user");
    	}
    	else
    	{
    		System.out.println("valid user");
    		return new ModelAndView("homepage","msg","Welcome "+user1.getUserName());
    	}
    }*/
    @RequestMapping(value="/login",method=RequestMethod.GET)
    public String init(Model model)
    {
    	//model.addAttribute("user",new User());
    	return "login";
    }
    public String submit(Model model,@ModelAttribute("user") User user) {
        User user1 =  userDao.findByUserNameAndPassword(user.getUserName(),user.getPassword());
        if(user1==null)
             {
        	 model.addAttribute("error", "Invalid Details");
              return "login";
              }
         else{
             System.out.println("valid user");
             model.addAttribute("msg","Welcome "+user.getUserName());
             return "homepage";
             }
    }
    /*CrudRepository c;
    PagingAndSortingRepository p;*/
     
       
   /*@RequestMapping(value = "/create", method = RequestMethod.POST)
      
     public void createPet(){
      //@ResponseBody
     // public List<Pet> getAllPets1(){
          User user1 = new User(200,"veena","venaaa");
          userRepository.save(user1);
          Set<Pet> petss = new HashSet<Pet>();
          Pet pp = new Pet(12,"raju",4,"kl");
          petss.add(pp);
          pp.setUser(user1);          
         petRepository.save(pp);
         System.out.println("mapped two tables successfully");
   }*/
    @RequestMapping("/home")  
    public ModelAndView showHome(){  
         //command is a reserved request attribute name, now use <form> tag to show object data  
        return new ModelAndView("redirect:/pet/viewallpets","command",new Pet());  
    }  
    @RequestMapping("/addPet")  
    public String addPet(Model model){  
         //command is a reserved request attribute name, now use <form> tag to show object data  
        model.addAttribute("addPet", new Pet());
        return "addPet";  
        
    }  
    @RequestMapping(value = "/viewallpets",method = RequestMethod.GET)
    public String index(HttpServletRequest request, ModelMap modelMap) {
        List<Pet> pets = (List<Pet>)petDao.findAll();
        PagedListHolder pagedListHolder = new PagedListHolder(pets);
        int page = ServletRequestUtils.getIntParameter(request, "p", 0);
        pagedListHolder.setPage(page);
        pagedListHolder.setPageSize(5);
        modelMap.put("pagedListHolder", pagedListHolder);
        return "homepage";
     }
    /* @RequestMapping(value = "/viewmypets",method = RequestMethod.POST)
    public String myPets(HttpServletRequest request, ModelMap modelMap, @ModelAttribute("username") String username) {
  User u = userRepository.findUserById(username);
        List<Pet> pets = (List<Pet>)petRepository.findAllById(u.getOwnerId());
        
        PagedListHolder pagedListHolder = new PagedListHolder(pets);
        int page = ServletRequestUtils.getIntParameter(request, "p", 0);
        pagedListHolder.setPage(page);
        pagedListHolder.setPageSize(3);
        modelMap.put("pagedListHolder", pagedListHolder);
        return "viewMyPets";



    }
*/
    @RequestMapping(value="/savePet", method=RequestMethod.POST)
    public ModelAndView savePet(@ModelAttribute Pet pet){
        System.out.println("Pet Details Requested: All Pets");
        petDao.save();
        return new ModelAndView("redirect:/pet/viewallpets","command",new Pet());
    }
}

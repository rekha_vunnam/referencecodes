package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Pet;
import com.model.User;

public interface UserDao extends JpaRepository<User,Long>{
	//public void buyPet();
    //public List<Pet> getMyPets();
    //public void authenticateUser();
    public User save(User user);
    public List<User> findAll();
    @Query(value="select PeerUser.id from PeerUser where PeerUser.username=?", nativeQuery=true)
    public User findUserById(@Param("username") String username);
    @Query(value="select * from PeerUser user1 where user1.userpassword=:password and user1.username=:username",nativeQuery=true)
    public User findUserByName(@Param("password") String password,@Param("username") String username);
    public User findByUserNameAndPassword(String userName, String password);
}

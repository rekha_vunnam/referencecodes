package com.dependency;

public class Client {
	private Cheat cheat; //creating object for interface and adding setters for that one

	public Cheat getCheat() {
		return cheat;
	}

	public void setCheat(Cheat cheat) {
		this.cheat = cheat;
	}
	public void cheating()
	{
		cheat.cheat();
	}

}

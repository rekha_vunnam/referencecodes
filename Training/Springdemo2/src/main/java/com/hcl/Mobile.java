package com.hcl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Mobile {
	public static void main(String[] args)
	{
		ApplicationContext context=new ClassPathXmlApplicationContext("com/test/beans.xml");
		Sim sim=context.getBean("sim",Sim.class);
		sim.calling();
		sim.data();
	}

}

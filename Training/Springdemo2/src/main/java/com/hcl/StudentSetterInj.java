package com.hcl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StudentSetterInj {
	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("com/test/beans1.xml");
		Student student=context.getBean("stu3",Student.class);
		student.display();
	}

}

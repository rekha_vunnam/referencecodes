package com.annotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
	@Bean
	public Principal principalBean()
	{
		return new Principal(); //this line avoids Pricipal pric=new Principal();
	}
	@Bean
	public College collegeBean()
	{
		College college=new College();
		college.setPrinciple(principalBean());//setter injection
		return college;
		
	}

}

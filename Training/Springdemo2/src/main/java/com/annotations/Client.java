package com.annotations;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Client {
	public static void main(String[] args) {
		ApplicationContext context=new AnnotationConfigApplicationContext(Config.class);
		College college=context.getBean("collegeBean",College.class);
		System.out.println("The object is "+college);
		college.display();
		
	}

}

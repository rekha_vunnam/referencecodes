package com.annotations;

public class College {
	private Principal principle;

	public Principal getPrinciple() {
		return principle;
	}

	public void setPrinciple(Principal principle) {
		this.principle = principle;
	}
	

	public void display() {
		principle.info();
		System.out.println("This is testing");
		
		
	}

}

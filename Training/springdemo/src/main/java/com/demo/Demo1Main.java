//Introduction to springbasics

package com.demo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo1Main {
    public static void main(String[] args) 
    {
        //container---> life cycle of bean
        ApplicationContext context=new ClassPathXmlApplicationContext("/com/demo/spring-config.xml");
        Employee emp=(Employee) context.getBean("employee");
        Employee emp1=(Employee) context.getBean("employee");
        emp.setEmpName("Welcome to Spring");
        emp.setEmpId(12);
        System.out.println(emp.getEmpName());
        System.out.println(emp.getEmpId());
        emp1.setEmpName("Two");
        emp1.setEmpId(22);
        System.out.println(emp1.getEmpName());
        System.out.println(emp1.getEmpId());
        System.out.println("End");
    }


}
 







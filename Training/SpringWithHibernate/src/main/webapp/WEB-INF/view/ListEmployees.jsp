<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1 align="center" >ListofEmployees</h1>
<p>
<button onclick="window.location.href='add'";return false;>Add Employee</button>
</p>
<table border="1">
    <tr>
    <th>Name</th>
    <th>Email</th>
    <th>Gender</th>
    <th>Hobbies</th>
    <th>DateOfBirth</th>
    <th>Address</th>
    <th>Action</th>
    </tr>
    <c:forEach items="${theEmployee}" var="employee">
    <c:url var="updateLink" value="updateRecord">
    <c:param name="employeeId" value="${employee.id }"/>
    </c:url>
    <c:url var="deleteLink" value="deleteRecord">
    <c:param name="employeeId" value="${employee.id }"/>
    </c:url>
    <tr>
    <td>${employee.name}</td>
    <td>${employee.email}</td>
    <td>${employee.gender}</td>
    <td>${employee.hobbies}</td>
    <td>${employee.dateofbirth}</td>
    <td>${employee.address}</td>
    <td>
       <a href="${updateLink}">Update</a> | <a href ="${deleteLink}">Delete</a>
    </td>
    
    </c:forEach>

</table>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1 align="center">Add Employee</h1>
	<hr />
	<f:form modelAttribute="employee" action="save">
	<f:hidden path="id"/>
		<table>
			<tr>
				<td>Name</td>
				<td><f:input path="name" /></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><f:input path="email" /></td>
			</tr>
			<tr>
				<td>Gender</td>
				<td><f:radiobutton path="gender" value="Male" />Male
				<td><f:radiobutton path="gender" value="Female" />Female
				</td>
			</tr>
			<tr>
				<td>Hobbies</td>
				<td><f:checkbox path="hobbies"  value="Cricket" />Cricket<br/> 
				     <f:checkbox path="hobbies" value="Football" />Football<br/> 
				     <f:checkbox path="hobbies" value="Carroms" />Carroms<br/> 
				     <f:checkbox path="hobbies" value="Chess" />Chess<br/></td>
			</tr>
			<tr>
				<td>DateOfBirth</td>
				<td><f:input path="dateofbirth" type="date" /></td>
			</tr>
			<tr>
				<td></td>
			<tr>
				<td>Address</td>
				<td><f:textarea path="address" /></td>
			</tr>
			
				<td><input type="submit" value="Submit"> <input
					 type="reset" value="Reset"></td>
			</tr>
		</table>
	</f:form>
	<a href="listEmployees">Click here to go for list of employees</a>

</body>
</html>
package com.service;

import java.util.List;

import com.model.Employee;

public interface EmployeeService {

	public void saveEmployee(Employee employee);

	public List<Employee> getAllEmployees();

	public Employee getEmployeeObjById(Integer employeeId);

	public void deleteEmployeeObj(Employee employee);

}

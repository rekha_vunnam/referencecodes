package com.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.EmployeeDao;
import com.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	@Autowired
    private EmployeeDao employeeDao;
	@Override
	@Transactional//everytime we cant open and close connections
	public void saveEmployee(Employee employee) {
		employeeDao.saveEmployee(employee);
		
	}
	@Override
	@Transactional
	public List<Employee> getAllEmployees() {
		return employeeDao.getAllEmployees();
	}
	@Override
	@Transactional
	public Employee getEmployeeObjById(Integer employeeId) {
		return employeeDao.getEmployeeObjById(employeeId);
	}
	@Override
	@Transactional
	public void deleteEmployeeObj(Employee employee) {
		employeeDao.deleteEmployeeObj(employee);
		
	}

}

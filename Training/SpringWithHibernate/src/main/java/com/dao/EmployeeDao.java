package com.dao;

import java.util.List;

import com.model.Employee;

public interface EmployeeDao {

	public void saveEmployee(Employee employee);

	public List<Employee> getAllEmployees();

	public Employee getEmployeeObjById(Integer employeeId);

	public void deleteEmployeeObj(Employee employee);

}

package com.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Employee;

@Repository//to tell spring it is a repository class
public class EmployeeDaoImpl implements EmployeeDao{
	@Autowired
    private SessionFactory sessionFactory;//to save into database we use sessionfactory
	@Override
	public void saveEmployee(Employee employee) {
		sessionFactory.getCurrentSession().saveOrUpdate(employee);
		
		
		
	}
	@Override
	public List<Employee> getAllEmployees() {
		return sessionFactory.getCurrentSession().createQuery("from Employee").list();
	}
	@Override
	public Employee getEmployeeObjById(Integer employeeId) {
		Employee employee=(Employee) sessionFactory.getCurrentSession().get(Employee.class,employeeId);
		return employee;
		
	}
	@Override
	public void deleteEmployeeObj(Employee employee) {
		sessionFactory.getCurrentSession().delete(employee);
		
	}

}

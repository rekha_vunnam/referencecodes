package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.Employee;
import com.service.EmployeeService;

@Controller
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;
	@RequestMapping("/add")
	public String display(Model model)
	{
		Employee employee=new Employee();
		model.addAttribute("employee",employee );
		return "AddEmployee";
	}
		@RequestMapping("/save")
		public String save(@ModelAttribute("employee")Employee employee,Model model)
		{
			employeeService.saveEmployee(employee);
			List<Employee>listOfEmployees=employeeService.getAllEmployees();
			model.addAttribute("theEmployee",listOfEmployees);
			return  "ListEmployees";
		}
		@RequestMapping(value="/listEmployees")
		public String getAllEmployees(Model model) {
			
			List<Employee>listOfEmployees=employeeService.getAllEmployees();
			model.addAttribute("theEmployee",listOfEmployees);
			return "ListEmployees";
		}
		@RequestMapping(value="updateRecord")
		public String showUpdateForm(@RequestParam("employeeId") Integer employeeId,Model model)
		{
			Employee employee=findById(employeeId);
			model.addAttribute("employee",employee);
			return "AddEmployee";
		}
		private Employee findById(Integer employeeId)
		{
			return employeeService.getEmployeeObjById(employeeId);
		}
		@RequestMapping(value="deleteRecord")
		public String deleteEmployeeObj(@RequestParam("employeeId") Integer employeeId)
		{
			Employee employee=findById(employeeId);
			employeeService.deleteEmployeeObj(employee);
			//model.addAttribute("employee",employee);
			return "redirect:/listEmployees";
		
		}
		
	}



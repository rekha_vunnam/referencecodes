package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {
	//@RequestMapping("/")
	@GetMapping(value= {"/abc","efg"})//multiple url
	public String firstFunction()
	{
		return "anyname";
	}
	@GetMapping(value="/hij")
	public String secondFunction(@RequestParam("userName")String val)
	{
		return "anyname";
	}
	@GetMapping(value="lmn/{alias}")
	public String ThirdFunction(@PathVariable("alias")String val)
	{
		return "anyname";
	}
	//carry data from controller to JSP page
	@GetMapping(value="carry")
	public String fourthFunction(Model model)   //model is part of my spring
	{
		model.addAttribute("key1", "Hard coding");
		return "success";
	}

}

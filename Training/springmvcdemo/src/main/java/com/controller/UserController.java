package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.User;

@Controller
public class UserController {
	
	/*@PostMapping(value="/login")
	public String loginFunction(@ModelAttribute User user)//if we want to get requestparam or pathparam as object we use modelattribute.
	{
		System.out.println(user.getUserName());
		return "usersuccess";
	}*/
	@PostMapping(value="/login")
	/*public String loginFunction(@ModelAttribute User user , Model model)
	{
	System.out.println(user.getUserName());
	model.addAttribute("key2", user);
	return "usersuccess";
	}*/
	public ModelAndView loginFunction(@ModelAttribute User user )
	{
	System.out.println(user.getUserName());
	ModelAndView modelandView;
	if(user.getPassword().equals("welcome"))
	{
		modelandView=new ModelAndView("usersuccess");
	}
	else
	{
		modelandView=new ModelAndView("failure");
	}
	return modelandView;
	}
}


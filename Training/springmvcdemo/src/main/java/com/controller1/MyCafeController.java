package com.controller1;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyCafeController {
	//returns welcome page
	@RequestMapping("/cafe")
	public String showWelcomePage(Model model)
	{
		
		model.addAttribute("myWebsiteName", "MOM'S CAFEE");
		return "welcome";
	}
	@RequestMapping("/processOrder")
	public String processOrder(HttpServletRequest request,Model model)
	{
		//handle the data received from user
		String UserEntered=request.getParameter("foodType");
		model.addAttribute("userInput", UserEntered);
		return "process";//here process is called view.
	}

}

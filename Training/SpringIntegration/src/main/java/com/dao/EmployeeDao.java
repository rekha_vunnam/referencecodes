package com.dao;

import com.model.Employee;

public interface EmployeeDao {
	Employee readEmployeeById(int empNo);

}

package com.main;

public class Vegetable {
	private String myName;
	public Vegetable()
	{
		
	}
	public Vegetable(String myName) {
		super();
		this.myName = myName;
	}
	public String getMyName() {
		return myName;
	}
	public void setMyName(String myName) {
		this.myName = myName;
	}
	public String display()
	{
		String speech="hii i am  a vegetable";
		if(myName!=null && myName!="")
			speech=speech + "My name is: "+myName;
		return speech;
	}

}

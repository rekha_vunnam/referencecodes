package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class myApp {
	public static void main(String[] args)
	{
		/*Fruit fruit=new Fruit();
		Vegetable vegetable=new Vegetable();
		System.out.println(fruit.display());
		System.out.println(vegetable.display());*/
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/test/beans.xml");// FileSystemXmlApplicationContext("beans.xml");
		Fruit fruit = applicationContext.getBean("fruit", Fruit.class);
		Vegetable vegetable = applicationContext.getBean("vegetables", Vegetable.class);
		Fruit otherfruit = applicationContext.getBean("fruitWithName", Fruit.class);
		Vegetable myOtherVeg = applicationContext.getBean("vegWithName", Vegetable.class);
		Vegetable vegWithPNameSpace = applicationContext.getBean("vegUsingPNameSpace", Vegetable.class);
		
		//Fruit myfruit = applicationContext.getBean("fruitwithlistandmap", Fruit.class);
		System.out.println(fruit.display());
		System.out.println(vegetable.display());
		System.out.println(otherfruit.display());
		System.out.println(myOtherVeg.display());
		System.out.println(vegWithPNameSpace.display());
		//System.out.println(myfruit.display());
	}
	}

//Here if we used FileSystemXmlApplicationContext-it will search whther it is there in project ,it wont check inside any package
//So i added ClassPathXmlApplicationContext-Checks inside a given package(com/test/beans.xml)

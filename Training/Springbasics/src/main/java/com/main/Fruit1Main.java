package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Fruit1Main {

	public static void main(String[] args) {

		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/test/bean2.xml");// FileSystemXmlApplicationContext("bean2.xml");
		Fruit1 fruit1 = applicationContext.getBean("fruit1", Fruit1.class);
		System.out.println(fruit1.display());
		((ClassPathXmlApplicationContext)applicationContext).close();

	}

}

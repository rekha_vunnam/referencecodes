package com.main;

public class Fruit1 {
	private String description="not set";
	public Fruit1()
	{
		
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public String display() {
		return description;
	}
	public void initMethod()
	{
		System.out.println("The fruit "+this.description);
	}
	public void destroyMethod()
	{
		System.out.println("destroy");
	}

}

package com.main1;

import org.springframework.beans.factory.annotation.Autowired;

public class Meal {
	//@Autowired
	private Fruit fruit;
	//@Autowired
	private Grain grain;
	public Fruit getFruit() {
		return fruit;
	}
	public void setFruit(Fruit fruit) {
		this.fruit = fruit;
	}
	public Grain getGrain() {
		return grain;
	}
	public void setGrain(Grain grain) {
		this.grain = grain;
	}
	
	public Meal() {
		super();
	}
	@Autowired
	public Meal(Fruit fruit, Grain grain) {
		super();
		this.fruit = fruit;
		this.grain = grain;
	}
	public String display() {
		String answer="This meal contains ";
		if(fruit!=null)answer +=" some fruit";
		if(grain!=null)answer +=" some grain";
		return answer;
	}

}

package com.main1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Mainapp {
	public static void main(String[] args)
	{
		//ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/main1/bean1.xml");// FileSystemXmlApplicationContext("beans.xml");
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/main1/bean_annotation.xml");// FileSystemXmlApplicationContext("beans.xml");
		/*Fruit fruit = applicationContext.getBean("fruit", Fruit.class);
		Grain grain = applicationContext.getBean("grain", Grain.class);*/
		/*Meal meal=applicationContext.getBean("meal",Meal.class);
		System.out.println(meal.display());*/
		/*Meal autowire=applicationContext.getBean("mealAutowired",Meal.class);
		System.out.println("autowire" +autowire.display());*/
		Meal autowire=applicationContext.getBean("mealbyannotation",Meal.class);
		System.out.println("Spring meal: " +autowire.display());
		//applicationContext.close();
	}
      
}

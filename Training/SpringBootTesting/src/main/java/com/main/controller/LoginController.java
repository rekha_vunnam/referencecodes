package com.main.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.main.model.Login;

@RestController
public class LoginController {
	
	@GetMapping("/login")
    public ModelAndView loginPage()
    {
        return new ModelAndView("login");
    }
	@GetMapping("/register")
    public ModelAndView RegisterPage()
    {
        return new ModelAndView("register");
    }
	@GetMapping("/success")
    public ModelAndView SuccessPage()
    {
        return new ModelAndView("success");
    }
	@GetMapping("/hello")
    public ModelAndView WelcomePage(@ModelAttribute("hello")Login login)
    {
        return new ModelAndView("hello");
    }
	
}

package com.test;


//import org.junit.Test;
//import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
//import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
//@RunWith(SpringJUnit4ClassRunner.class)//Junit 5

@ExtendWith(SpringExtension.class)
@SpringBootTest                                     // Testcase inside Spring Boot
@ContextConfiguration(classes=TestBeanConfig.class) //componentscan
public class LoginControllerTest {
	@Autowired
	 WebApplicationContext context;
	 private MockMvc mvc;
	 @BeforeEach
	 public void setup() {
	     mvc = MockMvcBuilders.webAppContextSetup(context).build();
}
@Test
public void loginTest() throws Exception
{
	mvc.perform(MockMvcRequestBuilders.get("/login"))
	.andExpect(MockMvcResultMatchers.status().isOk())
	.andExpect(MockMvcResultMatchers.view().name("login"));
}
@Test
public void registerTest() throws Exception
{
	mvc.perform(MockMvcRequestBuilders.get("/register"))
	.andExpect(MockMvcResultMatchers.status().isOk())
	.andExpect(MockMvcResultMatchers.view().name("register"));
}
@Test
public void helloTest() throws Exception
{
	mvc.perform(MockMvcRequestBuilders.get("/hello"))
	.andExpect(MockMvcResultMatchers.status().isOk())
	.andExpect(MockMvcResultMatchers.view().name("hello"));
}
@Test
public void successTest() throws Exception
{
	mvc.perform(MockMvcRequestBuilders.get("/success"))
	.andExpect(MockMvcResultMatchers.status().isOk())
	.andExpect(MockMvcResultMatchers.view().name("success"));
}
}

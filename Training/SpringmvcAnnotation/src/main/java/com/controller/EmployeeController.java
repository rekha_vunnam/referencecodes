package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import com.model.Employee;
import com.service.EmployeeService;

@Controller
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;
	@GetMapping(value="/add")
	public String add(@ModelAttribute Employee employeeController,Model model)
	{
		Employee var=employeeService.addEmployee(employeeController);
		model.addAttribute(arg0)
	}
	@GetMapping(value="/read/{lias}")
	public String read(@PathVariable("alias")int id,Model model)
	{
		Employee temp=employeeService.readEmployee(id);
		model.addAttribute("key2",temp);
		return "readview";
	}
	@GetMapping(value="/all")
	public String displayAllEmployee(Model model)
	{
		List<Employee>employeeService.listEmployee();
		return "showAll";
	}
}

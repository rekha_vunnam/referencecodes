package com.service;

import java.util.ArrayList;
import java.util.List;

import com.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee addEmployee(Employee employee) {
		String var=employee.getEmpName();
		employee.setEmpName(var +"from service");
		return employee;
	}

	@Override
	public Employee readEmployee(int empId) {
		Employee employee=null;
		if(empId==10) {
			employee=new Employee(10,"Ten Name",1010f);
		}
		else if(empId==20)
		{
			employee=new Employee(20,"Twenty Name",2020f);
		}
		return employee;
	}

	@Override
	public List<Employee> listEmployee() {
		Employee employee1=new Employee(100,"Hundred",100f);
		Employee employee2=new Employee(200,"Two Hundred",200f);
		List<Employee> employees=new ArrayList();
		employees.add(employee1);
		employees.add(employee2);
		return null;
	}

}

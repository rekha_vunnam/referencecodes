package com.pack.SpringJpa1;

import java.util.List;

import org.springframework.data.domain.Sort;

//import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Sort;

//import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface EmployeeRepository extends CrudRepository<Employee,Integer>{
	//Derived Query
	
	List<Employee> findByDept(String deptname);
	List<Employee> findBySalaryGreaterThan(double salary);
	List<Employee> findByDeptAndSalaryLessThan(String deptName,double salary);
	
	//Limited Query
	
	Employee findTopByOrderBySalaryDesc();
	Employee findTopByOrderBySalaryAsc();
	List<Employee> findTop3ByOrderBySalaryDesc();
	List<Employee> findTop3ByOrderBySalaryAsc();
	List<Employee> findFirst2BySalary(double salary);
	List<Employee> findFirst2ByDeptOrderBySalaryDesc(String deptName);
	
	//LIKE Expression
	
	List<Employee> findByNameLike(String likeString);
	List<Employee> findByDeptLike(String likeString);
	
	//count query
	long countByDept(String deptName);
	long countBySalaryGreaterThanEqual(double salary);
	long countByNameEndingWith(String ensString);
	long countByNameLike(String likeString);
	//@Query("SELECT e.dept,MAX(e.salary) FROM Employee e Group BY e.dept HAVING e.dept in ?1")
	//public List<Object[]> findMaxSalariesByDept(List<String>deptNames);

	//Named Query     //(? is used incase if we want to pass parameters we have used ? after e.dept,so deptName acts as parameter)
	public List<Object[]> findMaxSalariesByDept(List<String> deptNames);
	
	@Query("SELECT e FROM Employee e WHERE e.name LIKE ?1")
	public List<Employee> findByName(String name);
	
	@Query("SELECT e FROM Employee e WHERE e.name LIKE %?1%")
	public List<Employee> findByName2(String name);
    public List<Employee>findByNameContaining(String name);
    
      //Native Query-->it is sql query bcse it queries on table not on entity
        @Query(value="Select * from emp1000 where id=?1",nativeQuery=true)
         public Employee findAllEmployee(Integer id);
        
          //NamedNativeQuery
          public List<Employee> findBySalary();
          
        //@Param
      	@Query("select e from Employee e where e.id=:eid")
      	public Employee findEmployee(@Param("eid")Integer empid);
      	
      	//#entityname
      	@Query("SELECT e FROM #{#entityName} e WHERE e.dept=?1")
      	public List<Employee> findByDepartment(String deptName);
      	
      	//using dml queries
      	@Transactional
      	@Modifying
      	@Query("UPDATE Employee e SET e.salary=e.salary+e.salary * :byPercent/100 WHERE e.dept=:dept")
      	int updateDeptSalaries(@Param("dept")String dept,@Param("byPercent")double byPercent);
        //sort
      	public List<Employee> findByDept(String deptName,Sort sort);
}



//we have to create this repository and we have to extend CrudRepository which contains save method,so there is no need to write multiple code.

//here we are using hql queries bcse here we are writing queries for entity class not for the table in sql(check employeetable name)


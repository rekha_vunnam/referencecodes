package com.pack.SpringJpa1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;

@SpringBootApplication
public class SpringJpa1Application implements CommandLineRunner {
	@Autowired
	EmployeeRepository repo;

	public static void main(String[] args) {
		SpringApplication.run(SpringJpa1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		 createEmployee();
		// findAllEmployees();//displays on console all employee data
		// checkEmployeeAndDelete(10);
		// updateEmployee(11);
		// derivedQueryInfo();
		// limitedQueryInfo();
		// likeExpressionInfo();
		//countQueryInfo();
		  //namedQueryJpa();
		//queryInfo();
		//nativeQueryDemo();
		//paramDemo();
		//modifyingDemo();
		//sortDemo();
		
		

	}

	private void sortDemo() {
		System.out.println("--finding by dept Sales sort by 'salary' and 'name'---");
		List<Employee> list= repo.findByDept("Sales",Sort.by("name","salary").ascending());
		list.forEach(System.out::println);
		
	}

	private void modifyingDemo() {
		int updateCount=repo.updateDeptSalaries("HR", 16);
		System.out.println("Update count: "+updateCount);
		System.out.println("--all employees after update--");
		repo.findAll().forEach(System.out::println);
		
	}

	private void paramDemo() {
		System.out.println("--find single employee--");
	 Employee emp=repo.findEmployee(15);
	 System.out.println(emp);
	 
	 //usage of #
	 System.out.println("using # to find entity class");
	 List<Employee> list=repo.findByDepartment("Admin");
	 list.forEach(System.out::println);
		
	}

	private void nativeQueryDemo() {
		System.out.println("--find single employee--");
		Employee emp=repo.findAllEmployee(15);
		System.out.println(emp);
		System.out.println("Native named query");
		List<Employee> list=repo.findBySalary();
		list.forEach(System.out::println);
	}

	private void queryInfo() {
		System.out.println("--finding by name containing %n--");
		List<Employee> list=repo.findByName("%n");
		 list.forEach(System.out::println);
		 System.out.println("--finding by name containing n--");
		 list=repo.findByName2("n");
		 list.forEach(System.out::println);
		 System.out.println("--finding via querymethod name containing %n--");
		 list=repo.findByNameContaining("n");
		 list.forEach(System.out::println);
		 
		
		
	}

	private void namedQueryJpa() {
		System.out.println("--finding max salaries in Admin and IT depts--");
		 List<Object[]> list = repo.findMaxSalariesByDept(Arrays.asList("Admin", "HR"));
         list.forEach(arr -> {
             System.out.println(Arrays.toString(arr));
         });
		
	}

	private void countQueryInfo() {

		System.out.println(" -- finding the employee count in HR dept --");
		long count = repo.countByDept("HR");
		System.out.println(count);

		System.out.println(" -- finding the employee count with salary greater or equal to 4000  --");
		count = repo.countBySalaryGreaterThanEqual(30000.0);
		System.out.println(count);

		System.out.println(" -- finding the employee count with name ending with 'e'  --");
		count = repo.countByNameEndingWith("e");
		System.out.println(count);

		System.out.println(" -- finding the employee count with name like '%a_a' --");
		count = repo.countByNameLike("R%");
		System.out.println(count);
	}

	private void likeExpressionInfo() {
		System.out.println("--finding employee with name like %ana--");
		List<Employee> list = repo.findByNameLike("R%");
		list.forEach(System.out::println);

		System.out.println(" -- finding employee with dept name more than 3 chars --");
		list = repo.findByDeptLike("___%");
		list.forEach(System.out::println);

	}

	private void limitedQueryInfo() {
		System.out.println(" -- finding the employee with max salary --");
		Employee emp = repo.findTopByOrderBySalaryDesc();
		System.out.println(emp);

		System.out.println(" -- finding the employee with min salary --");
		emp = repo.findTopByOrderBySalaryAsc();
		System.out.println(emp);

		System.out.println(" -- finding the top 3 employees with max salary --");
		List<Employee> list = repo.findTop3ByOrderBySalaryDesc();
		list.stream().forEach(System.out::println);

		System.out.println(" -- finding the top 3 employees with min salary --");
		list = repo.findTop3ByOrderBySalaryAsc();
		list.stream().forEach(System.out::println);

		System.out.println(" -- finding the first 2 employees with salary 30000 --");
		list = repo.findFirst2BySalary(30000.0);
		list.stream().forEach(System.out::println);

		System.out.println(" -- finding the top 2 employees with max salary in HR dept --");
		list = repo.findFirst2ByDeptOrderBySalaryDesc("HR");
		list.stream().forEach(System.out::println);

	}

	private void derivedQueryInfo() {
		System.out.println("--finding by dept--");
		List<Employee> list = repo.findByDept("HR");
		list.stream().forEach(System.out::println); // we used here streams with foreach

		System.out.println(" -- finding by salary greater than 3000 --");
		List<Employee> list2 = repo.findBySalaryGreaterThan(30000.0);
		list2.stream().forEach(System.out::println);

		System.out.println(" -- finding by dept=HR and salary less than 40000 --");
		List<Employee> list3 = repo.findByDeptAndSalaryLessThan("HR", 40000.0);
		list3.stream().forEach(System.out::println);
	}

	private void updateEmployee(int id) {
		if (repo.existsById(id)) {
			System.out.println("--updating--");
			Employee employee2 = repo.findById(id).orElseThrow(RuntimeException::new); // if the employee with
																						// particular id is not present
																						// then it will throw runtime
																						// error.
			System.out.println(employee2);
			employee2.setSalary(55000.0);
			repo.save(employee2);
		}

	}

	private void checkEmployeeAndDelete(int id) {
		if (repo.existsById(id)) // existById is used to check whether the employee is present or not
		{
			System.out.println("---finding by id--");
			Optional<Employee> employeeOptional = repo.findById(id); // optional is used to avoid nullpointer exception.
			Employee employee = employeeOptional.orElseThrow(RuntimeException::new);
			System.out.println(employee);
			System.out.println("--deleting--");
			System.out.println(employee);
			repo.delete(employee);
		}

	}

	private void findAllEmployees() {
		Iterable<Employee> all = repo.findAll();
		System.out.println("All Employees: ");
		all.forEach(item -> System.out.println(item));

	}

	private void createEmployee() {
		Employee e1 = new Employee(10, "Ram", "male", "ram@gmail.com", 20000.0, "HR");
		repo.save(e1);// to save singleobject and this save method is present in crudRepository to
						// avoid boilercode
		List<Employee> list = new ArrayList<>();
		Employee e2 = new Employee(11, "Sam", "male", "sam@gmail.com", 30000.0, "Finance");
		list.add(e2);
		Employee e3 = new Employee(12, "Penny", "female", "penny@gmail.com", 35000.0, "Admin");
		list.add(e3);
		Employee e4 = new Employee(14, "Rajesh", "male", "rajesh@gmail.com", 40000.0, "HR");
		list.add(e4);
		Employee e5 = new Employee(15, "Suchi", "female", "suchi@gmail.com", 45000.0, "Sales");
		list.add(e5);
		Employee e6 = new Employee(11, "Samuel", "male", "samuel@gmail.com", 35000.0, "HR");
		list.add(e6);
		repo.saveAll(list);// save multiple employee object

	}

}

// CommandLineRunner is used to store into database and it is executed after
// main

package com.hcl.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

 

@Configuration
@EnableWebSecurity
public class LoginSecurityConfig extends WebSecurityConfigurerAdapter
{
    //authorization and authentication
    //used for authentication
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        
        auth.inMemoryAuthentication().withUser("user").password("abcd").roles("USER");
         auth.inMemoryAuthentication().withUser("admin").password("abcd1").roles("ADMIN");
   }
  //used for authorization
    public void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().antMatchers("/Success").access("hasRole('ROLE_USER')")
        .antMatchers("/admin").access("hasRole('ROLE_ADMIN')")
        .and().formLogin().loginPage("/loginPage")
                .successHandler(new CustomHandler()).failureUrl("/loginPage?error")
                .and().logout().logoutSuccessUrl("/loginPage?logout").and().exceptionHandling()
                .accessDeniedPage("/403");
 

    }
    
}
package com.pack.SpringBoot4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController {
	@Autowired
	HelloInterface hello;
	@Value("${myProperty}") //This is used to take the value from application.properties
	String message;
	@RequestMapping("/")
	public String hello(Model model)
	{
		String greeting=message+"from"+hello.getGreeting();
		model.addAttribute("greeting", greeting);
		return greeting;
	}

}

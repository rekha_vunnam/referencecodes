package com.pack.SpringBoot4;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component //registered as springbean program
@Profile("dev")
public class HelloDev implements HelloInterface{
	
	@Override
	public String getGreeting() {
		return "Hello from Dev";
	}

}

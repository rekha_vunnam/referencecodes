package com.pack.SpringBootJpa.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.pack.SpringBootJpa.model.Student;

public interface StudentDao extends CrudRepository<Student,Integer>{
 @Query(value="select * from stud100 limit :pageid,:total", nativeQuery=true)
 public List<Student> getStudentsByPage(@Param("pageid")int pageid,@Param("total")int total);
}
//in dao we need not to add deleteall method because crudrepositort contains deleteall method by predefined.

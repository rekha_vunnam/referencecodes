package com.pack.SpringBootJpa.service;

import java.util.List;

import com.pack.SpringBootJpa.model.Student;

public interface StudentService {
	public void saveStudent(Student student);

	public List<Student> getStudentsByPage(int pageId, int total);
	public long count();

	public void deleteAllStudent();

	public Student getStudentById(Integer studentId);

	public void updateStudent(Student student);

	public void deleteStudent(Integer sid);

}

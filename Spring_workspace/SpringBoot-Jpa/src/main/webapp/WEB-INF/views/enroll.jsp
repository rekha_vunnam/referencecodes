<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student Registration Form</title>
<link
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	rel="stylesheet">
<style>
.has-error{
	 color:red;
</style>
</head>
<body>
	<div class="container-fluid">
		<h2>Student Enrollment Form</h2>
		<form:form action="save" method="post" class="form-horizontal"
			modelAttribute="student">
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="firstName">Firstname</label>
					<div class="col-md-7">
						<form:input type="text" path="firstName" id="firstName"
							class="form-control input-sm" />
						<div class="has-error">
							<form:errors path="firstName" class="help-inline"></form:errors>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="lastName">Lastname</label>
					<div class="col-md-7">
						<form:input type="text" path="lastName" id="lastName"
							class="form-control input-sm" />
						<div class="has-error">
							<form:errors path="lastName" class="help-inline"></form:errors>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="email">Email</label>
					<div class="col-md-7">
						<form:input type="email" path="email" id="email"
							class="form-control input-sm" />
						<div class="has-error">
							<form:errors path="email" class="help-inline"></form:errors>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="dob">Dob</label>
					<div class="col-md-7">
						<form:input type="date" path="dob" id="dob"
							class="form-control input-sm" />
						<div class="has-error">
							<form:errors path="dob" class="help-inline"></form:errors>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="section">Section</label>
					<div class="col-md-7" class="form-control input-sm">
						<form:radiobuttons path="section" items="${sections}" />
						<div class="has-error">
							<form:errors path="section" class="help-inline"></form:errors>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="section">Section</label>
					<div class="col-md-7" class="form-control input-sm">
						<form:select path="country" id="country">
							<form:option value="">Select Country</form:option>
							<form:options items="${countries}" />
						</form:select>
						<div class="has-error">
							<form:errors path="country" class="help-inline"></form:errors>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="subjects">Subjects</label>
					<div class="col-md-7" class="form-control input-sm">
						<form:select path="subjects" items="${subjects}" />
						<div class="has-error">
							<form:errors path="subjects" class="help-inline"></form:errors>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="sex">Gender</label>
					<div class="col-md-7" class="form-control input-sm">
						<form:radiobutton path="sex" value="M" />
						Male
						<form:radiobutton path="sex" value="F" />
						Female
						<div class="has-error">
							<form:errors path="sex" class="help-inline"></form:errors>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="sex">FirstAttempt</label>
					<div class="col-md-1" class="form-control input-sm">
						<form:checkbox path="firstAttempt" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-actions floatRight">
					<input type="submit" value="Register"
						class="btn btn-primary btn-sm">
				</div>
			</div>
		</form:form>
	</div>
</body>
</html>
package com.pack.SpringBoot1;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBoot1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot1Application.class, args);
		ApplicationContext ctx=SpringApplication.run(SpringBoot1Application.class);
		String[] beanNames=ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for(String beanName:beanNames)
		{
			System.out.println(beanName);
		}
				
	}

}

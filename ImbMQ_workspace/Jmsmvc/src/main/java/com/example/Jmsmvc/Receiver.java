package com.example.Jmsmvc;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver {
   
    @JmsListener(destination="inmemory.queue1")
    public void getMessageForReceiver1(String message) {
        System.out.println("received by first receiver : "+message);
    }
   
    @JmsListener(destination="inmemory.queue2")
    public void getMessageForReceiver2(String message) {
        System.out.println("received by second receiver : "+message);
    }
}

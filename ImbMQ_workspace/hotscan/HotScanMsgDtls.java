/*************************************************************************************************************
-- Name         :  ChqEmbargoFilterPut.java
-- Description  :  This java program defines methods to connect Hotscan MQ server and put messages.
-- Author       :  Bala
-- Date 	    :  09/09/2008
--
-- Date		   Modidified By	SIR No		    Description
-- 09/09/2008   Bala            CHQ2921         Initial Version
**************************************************************************************************************/

package hotscan;

public class HotScanMsgDtls {

	private String msgText;
	private String transactionId;
	
	public HotScanMsgDtls(String msgText, String transactionId) {
		super();
		this.msgText = msgText;
		this.transactionId = transactionId;
	}

	public String getMsgText() {
		return msgText;
	}

	public void setMsgText(String msgText) {
		this.msgText = msgText;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	
	
}

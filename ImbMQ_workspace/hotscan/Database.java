/*************************************************************************************************************
-- Name         :  ChqEmbargoFilterPut.java
-- Description  :  This java program defines methods to connect Hotscan MQ server and put messages.
-- Author       :  Bala
-- Date 	    :  08/09/2008
--
-- Date		   Modidified By	SIR No		    Description
-- 08/09/2008   Bala            CHQ2921         Initial Version
-- 17/09/2012	Siva			AP71dbcq#1696	JDBC driver change due to 11g migration
**************************************************************************************************************/

package hotscan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;

//Changed jdbc driver by AP71dbcq#1696
//import oracle.jdbc.driver.OracleCallableStatement;
import oracle.jdbc.OracleCallableStatement;

import org.apache.commons.logging.Log;

public class Database {

	private static Log log = null;
	
	static
	{
		try
		{
		   Class.forName("oracle.jdbc.driver.OracleDriver");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	/* Method to setup the connection to the database */
	public static Connection setupConn(String dbUsrId, String dbPasswd, String dbSid, String unixHostName, String dbPort, String branchCode) {
				try {
					log = Util.initializeLogFile(branchCode,"Database.class");
					dbchqutil.StringEncPP pp = new dbchqutil.StringEncPP();
					String decryptPasswd = pp.getDec(dbPasswd, null);
					System.out.println(decryptPasswd);
					String driverType = "jdbc:oracle:thin:@";
					StringBuffer connectString 	= new StringBuffer();
					connectString.append(driverType+unixHostName+":"+dbPort+":"+dbSid);
					Connection conn = DriverManager.getConnection(connectString.toString(),dbUsrId,decryptPasswd);
					return conn;
			} 	catch (Exception e){
					log.warn("\nError connecting to the database.\n"+e.toString());
					return null;
			}
	}
	
	/* Method to get queue details from the database*/
	public static String[] getQueueDetailsFromDB(Connection conn, String moduleID, String branchCode, String ioRetStatus) {
				try {
						OracleCallableStatement oracallbleStmt = (OracleCallableStatement)conn.prepareCall("begin cqk_c_mq_procs.cqp_p_get_qm_detl(?,?,?,?); end;");
						oracallbleStmt.registerOutParameter(1,Types.VARCHAR);
						oracallbleStmt.setString(1,ioRetStatus);
						oracallbleStmt.setString(2,moduleID);
						oracallbleStmt.setString(3,branchCode);
						oracallbleStmt.registerIndexTableOutParameter(4,20,Types.VARCHAR,70);
						oracallbleStmt.execute();
						ioRetStatus=(String)oracallbleStmt.getString(1);
						if (!ioRetStatus.equals("0")) {
							throw new SQLException();
						}
						String[] qDtls = (String[])oracallbleStmt.getPlsqlIndexTable(4);
						return qDtls;
				} catch (Exception e) {
						log.warn("\nError while retreiving Hotscan queue details from P099...\n"+e.toString());
						return null;
				}
		}
	

}

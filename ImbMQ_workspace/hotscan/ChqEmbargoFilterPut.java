/*************************************************************************************************************
-- Name         :  ChqEmbargoFilterPut.java
-- Description  :  This java program defines methods to connect Hotscan MQ server and put messages.
-- Author       :  Bala
-- Date 	    :  08/09/2008
--
-- Date		   Modidified By	SIR No		   Description
-- 08/09/2008   Bala                 CHQ2921             Initial Version
-- 17/09/2012	  Siva			     AP71dbcq#1696	JDBC driver change due to 11g migration
-- 01/08/2013    manoj               PM0081748         passing ccsid for fixing Thai chars issue
**************************************************************************************************************/

package hotscan;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//Changed jdbc driver by AP71dbcq#1696
//import oracle.jdbc.driver.OracleCallableStatement;
import oracle.jdbc.OracleCallableStatement;
//import oracle.jdbc.driver.OracleTypes;
import oracle.jdbc.OracleTypes;

import org.apache.commons.logging.Log;

import com.ibm.mq.MQQueue;

public class ChqEmbargoFilterPut {

	private static Log log = null;
	private static String processingStatus=null;
	
	// Argument 1 = Database Username
	// Argument 2 = Database Encrypted Password
	// Argument 3 = Oracle SID
	// Argument 4 = Branch code
	// Argument 5 = Log Directory
	// Argument 6 = Hostname
	// Argument 7 = Oracle Port
	// Argument 8 = CCSID
	public static void main(String[] argv) {
	if(argv.length==8) {
      ChqEmbargoFilterPut obj_PutObj = new ChqEmbargoFilterPut();
      log = Util.initializeLogFile(argv[3],"ChqEmbargoFilterPut.class");
      String returnVal = obj_PutObj.mqHotscanPut(argv[0],argv[1],argv[2],argv[3],argv[4], argv[5], argv[6], argv[7]);
      System.out.println(returnVal);
	} else {
		System.out.println("Insufficient number of arguments");
	}
}
	
/* The wrapper program for online put calls */
public String mqHotscanPut(String dbUserID, String dbPasswd, String dbSid, String branchCode, String logDir, String unixHostName, String dbPort, String ccsid) {
		try {
			String qDtlsIORet = new String();
			String procMsgIOretVal = new String();
			Connection conn = Database.setupConn(dbUserID,dbPasswd,dbSid, unixHostName, dbPort, branchCode);
			List msgList = this.getUnprocMessagesFromDB(conn,branchCode,procMsgIOretVal);
			if(msgList.size()==0) {
				log.warn("\nNo messages available to send to Hotscan");
			} else {			
				String[] qDtls = Database.getQueueDetailsFromDB(conn,Constants.htscnQdtls,branchCode,qDtlsIORet);
				MQInterface.initializeQueueManager(qDtls, branchCode);
				MQQueue queue = MQInterface.initializeQueue(qDtls[1],Constants.sendQ);
				int nccsid=Integer.parseInt(ccsid);
				String[] strProccessedTrnID = MQInterface.putMessagesOntoQueue(queue,msgList,nccsid);			
				MQInterface.disconnectFromQueue(queue);
				MQInterface.disconnectQueueManager();
				this.updateMessageStatus(conn, branchCode,logDir,strProccessedTrnID, "P");
				int countSuccessMsg = strProccessedTrnID.length;
				if (msgList.size()!= countSuccessMsg) {
					int i = countSuccessMsg - 1;
					while (i>=0){
						msgList.remove(i);
						i--;
					}
					String[] strUnprocessedTrnID = new String[msgList.size()];
					int j=0;
					Iterator  it = msgList.listIterator();
					while(it.hasNext()) {
						HotScanMsgDtls msg = (HotScanMsgDtls) it.next();
						String transactionId = msg.getTransactionId();
						strUnprocessedTrnID[j] = transactionId;
						j++;
						}
					this.updateMessageStatus(conn, branchCode,logDir,strUnprocessedTrnID, "E");
				}
			}
			conn.close();
			return "0";
		} catch(Exception e) {
			if (this.getProcessingStatus().equals(Constants.noDataFound)){
				log.warn("\nNo records to available to send to Hotscan");
				return "0";
			}
			log.warn("\nException while calling the method 'mqHotscanPut'...");
			return("-1");
			}
	}

/* Method to get all unprocessed messages from the database*/
	public List getUnprocMessagesFromDB(Connection conn,String branchCode,String ioRetStatus) {
		try {
			OracleCallableStatement oracallbleStmt = (OracleCallableStatement)conn.prepareCall("begin cqk_p_hotscan_interface.cqp_send_hotscan_msg(?,?,?); end;");
			oracallbleStmt.registerOutParameter(1,Types.VARCHAR);
			oracallbleStmt.registerOutParameter(3,OracleTypes.CURSOR);
			oracallbleStmt.setString(1,ioRetStatus);
			oracallbleStmt.setString(2,branchCode);
			oracallbleStmt.execute();
			ioRetStatus=(String)oracallbleStmt.getString(1);
			if (!ioRetStatus.equals("0")) {
					throw new SQLException();
			}
			List resultsetMsgList = new ArrayList();
			ResultSet rsetSndMsgTxt = (ResultSet)oracallbleStmt.getObject(3);
			if (null==rsetSndMsgTxt)
			{
			    this.setProcessingStatus(Constants.noDataFound);
				log.warn("\nGetting the Status NO DATA FOUND");
				return null;
			}
			while (rsetSndMsgTxt.next()) {
				    String msgText = rsetSndMsgTxt.getString(1);
					String transactionId = rsetSndMsgTxt.getString(2);
					HotScanMsgDtls msgDet = new HotScanMsgDtls(msgText,transactionId);
					resultsetMsgList.add(msgDet);
			}
			rsetSndMsgTxt.close();
			return resultsetMsgList;
		}
		catch (SQLException sqle){
			if (sqle.getMessage().equals("Cursor is closed.")) {
				this.setProcessingStatus(Constants.noDataFound);
				log.warn("\nGetting the Status " + this.getProcessingStatus());
				return null;
		    }
			log.warn("\nSQL exception while retreiving unproccessed messages from CQT_C_HOTSCAN_RQST table...\n"+ "\nOracle Error Code: " + sqle.getErrorCode() + "\nOracle Error Message: " + sqle.getMessage());
			sqle.printStackTrace();
			return null;
		}
		catch (Exception e) {
			log.warn("\nError while retreiving unproccessed messages from CQT_C_HOTSCAN_RQST table...\n"+e.toString());
			e.printStackTrace();
			return null;
		}
	}

/*Update the processed messages*/
  public void updateMessageStatus(Connection conn, String branchCode, String strLogDir,String[] strProccessedTrnID, String sentStatus) {
			try {
				String ioRetStatus = new String();
				OracleCallableStatement oracallbleStmt = (OracleCallableStatement)conn.prepareCall("begin cqk_p_hotscan_interface.cqp_upd_hotscan_msg(?,?,?,?,?); end;");
				oracallbleStmt.registerOutParameter(1,Types.VARCHAR);
				oracallbleStmt.setString(1,ioRetStatus);
				oracallbleStmt.setString(2,branchCode);
				oracallbleStmt.setString(3,strLogDir);
				oracallbleStmt.setString(4,sentStatus);
				oracallbleStmt.setPlsqlIndexTable(5, strProccessedTrnID, strProccessedTrnID.length, strProccessedTrnID.length, OracleTypes.VARCHAR, 22);
				oracallbleStmt.execute();
				ioRetStatus=(String)oracallbleStmt.getString(1);
				if (!ioRetStatus.equals("0")) {
					throw new SQLException();
				}
			}catch(Exception e) {
				log.warn("\nError while updating the message status.\n"+e.toString());
			}
}

public static String getProcessingStatus() {
	return processingStatus;
}

public static void setProcessingStatus(String processingStatus) {
	ChqEmbargoFilterPut.processingStatus = processingStatus;
}





}
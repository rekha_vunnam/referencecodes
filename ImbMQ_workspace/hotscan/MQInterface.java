/*************************************************************************************************************
-- Name         :  ChqEmbargoFilterPut.java
-- Description  :  This java program defines methods to connect Hotscan MQ server and put messages.
-- Author       :  Bala
-- Date 	    :  09/09/2008
--
-- Date		   Modidified By	SIR No		    Description
-- 09/09/2008   Bala            CHQ2921         Initial Version
-- 07/10/2010	  Hari			CHQ3129			Parameter change due to request from Hotscan
-- 01/08/2013    manoj         PM0081748      passing ccsid for fixing Thai chars issue
**************************************************************************************************************/

package hotscan;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;

import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;

public class MQInterface {

	private static MQQueueManager qManager;
	
	private static Log log = null;
	
	/* Method to set the SSL System properties for the keystore and password, to enable Java to connect to the key repository*/
 	public static void setSSLSystemProperties(String sslKeyStore, String sslPassword) {
			try {
					System.setProperty("javax.net.ssl.trustStore",sslKeyStore);
					System.setProperty("javax.net.ssl.keyStore",sslKeyStore);
					System.setProperty("javax.net.ssl.trustStorePassword",sslPassword);
					System.setProperty("javax.net.ssl.keyStorePassword",sslPassword);
				} catch(Exception e) {
					log.warn("\nJava error while setting the MQ SSL properties.\n"+e.toString());
				}
	}
	
	/* Method to initialize the queue*/
	public static MQQueue initializeQueue(String queueName, String sndORrcvQ) {
			try {
					int queueOpenOptions=0;
					if (sndORrcvQ == Constants.rcvQ){
						queueOpenOptions = MQC.MQOO_INPUT_AS_Q_DEF;
					}
					if (sndORrcvQ == Constants.sendQ){
						queueOpenOptions = MQC.MQOO_OUTPUT; 
					}
					MQQueue queue;
					queue		= qManager.accessQueue(queueName, queueOpenOptions, null, null, null);
					return queue;
			} catch(MQException mqEx) {
					log.warn("\nMQSeries Error while connecting to the DbusMQ queue. Completion code :" +mqEx.completionCode + " Reason code :" + mqEx.reasonCode + " Cause: " + mqEx.getCause());
					return null;
			} catch(Exception e) {
					log.warn("\nGeneral Error while connecting to the queue.\n"+e.toString());
					return null;
			}
		}
	
	/* Method to initialize the queue manager*/
		public static void initializeQueueManager(String[] queueDtls, String branchCode) {
			try {
					String htscnQMgr			= queueDtls[0];
					String htscnSendQueue		= queueDtls[1];
					String htscnrcvQueue		= queueDtls[2];
					String htscnChannel			= queueDtls[3];
					String htscnHost				= queueDtls[6];
					String htscnSSLCipherSuite	= queueDtls[13];
					setSSLSystemProperties(queueDtls[12],"passw0rd");
					Integer port 				= new Integer(queueDtls[4]);
					int int_Port				= port.intValue();
					log = Util.initializeLogFile(branchCode,"MQInterface.class");
					
					log.warn("\n\n*******************************************");
					log.warn("\nDbusMQ Queue Manager : "+htscnQMgr);
					log.warn("\nDbusMQ Send Queue : "+htscnSendQueue);
					log.warn("\nDbusMQ Receive Queue : "+htscnrcvQueue);
					log.warn("\nDbusMQ Channel : "+htscnChannel);
					log.warn("\nDbusMQ Host IP Address : "+htscnHost);
					log.warn("\nDbusMQ Host Port Number : "+int_Port);
					log.warn("\nDbusMQ SSL Cipher Suite : "+htscnSSLCipherSuite);

					MQEnvironment.hostname 		   = htscnHost ;
					MQEnvironment.channel  		   = htscnChannel;
					MQEnvironment.port 			   = int_Port;
					MQEnvironment.sslCipherSuite   = htscnSSLCipherSuite;
					MQEnvironment.disableTracing();
					MQException.log				   = null;
					qManager	= new MQQueueManager(htscnQMgr);

			} catch(MQException mqEx) {
					log.warn("\nMQSeries Error while connecting to the queue manager. Completion code :" +mqEx.completionCode + " Reason code :" + mqEx.reasonCode + " Cause: " + mqEx.getCause());
			} catch(Exception e) {
					log.warn("\nGeneral Error while connecting to the queue manager.\n"+e.toString());
			}
		}
		
		/* Method to put messages onto the queue*/
		public static String[] putMessagesOntoQueue(MQQueue queue, List msgList, int ccsid) {
			int count= msgList.size();
			String[] strProccessedTrnID = new String[count];
			int i=0;
			try {
			Iterator  it = msgList.listIterator();

			while(it.hasNext()) {
				HotScanMsgDtls msg = (HotScanMsgDtls) it.next();
				String msgText = msg.getMsgText();
				String transactionId = msg.getTransactionId();
				MQMessage mqMessage	= new MQMessage();
				MQPutMessageOptions putMessageOptions = new MQPutMessageOptions();
				/*CHQ3129 - Hari - Code changes begin*/
				/*Included below parameters while putting messages on queue*/
				mqMessage.format = MQC.MQFMT_STRING;
				/*PM0081748 - removing hardcoded ccsid and passing from shell script*/
				/*mqMessage.characterSet = 819; */
				mqMessage.characterSet = ccsid;
				/*CHQ3129 - Hari - Code changes ends*/
				mqMessage.writeBytes(msgText.toString());
				queue.put(mqMessage,putMessageOptions);
				log.warn("\nTransaction ID : "+transactionId.toString() + " CCSID :" + ccsid); 
				strProccessedTrnID[i] = transactionId;
				i++;
				}
				return strProccessedTrnID;
			} catch(MQException mqEx) {
					log.warn("\nMQSeries Error while writing messages to the queue. Completion code :" +mqEx.completionCode + " Reason code :" + mqEx.reasonCode + " Cause: " + mqEx.getCause());
					return strProccessedTrnID;
			} catch(Exception e) {
					log.warn("\nGeneral Error while writing messages to the queue.\n"+e.toString());
					return strProccessedTrnID;
			}
	}

		/* Method to get all messages from the queue*/
		public static List getMessagesFromQueue(MQQueue queue) {
			List getMsgList = new ArrayList();
			try {
				while(true) {
					try {
						MQMessage mqMessage			= new MQMessage();
						MQGetMessageOptions getMessageOptions = new MQGetMessageOptions();
						queue.get(mqMessage,getMessageOptions);
						mqMessage.messageId			= MQC.MQMI_NONE;
						mqMessage.correlationId		= MQC.MQCI_NONE;
						int msgLength				= mqMessage.getMessageLength();
						String ls_Msg 				= mqMessage.readStringOfByteLength(msgLength);
						getMsgList.add(ls_Msg);
						log.warn("\nMessage : "+ls_Msg.toString());
					} catch(MQException mqEx) {
							if (mqEx.reasonCode == mqEx.MQRC_NO_MSG_AVAILABLE) {
									log.warn("\nNo more messages on the queue. Completion code :" +mqEx.completionCode + " Reason code :" + mqEx.reasonCode + " Cause: " + mqEx.getCause());
							} else {
									log.warn("\nMQSeries Error while getting messages fromthe queue. Completion code :" +mqEx.completionCode + " Reason code :" + mqEx.reasonCode + " Cause: " + mqEx.getCause());
							}
							break;
					} catch(Exception e) {
							log.warn("\nGeneral Error while getting messages from the queue.\n"+e.toString());
							break;
					}
			}
			return getMsgList;
		} catch(Exception e) {
				log.warn("\nGeneral Error while getting messages from the queue.\n"+e.toString());
				return getMsgList;
			}
		}

		/* Method to disconnect from the queue */
		public static void disconnectFromQueue(MQQueue queue) {
				try {
					if(queue.isOpen())
                       queue.close();
				} catch (MQException mqEx) {
							log.warn("\nMQSeries Error while disconnecting : Completion code " +mqEx.completionCode + " Reason code " + mqEx.reasonCode + " Cause: " + mqEx.getCause());
				} catch (Exception e) {
							log.warn("\nGeneral Error while disconnecting.\n " + e.toString());
				}
			}
		
		/* Method to disconnect from the queue manager */
		public static void disconnectQueueManager() {
			try {
					if(qManager.isConnected()){
						qManager.backout();
						qManager.disconnect();
					}
			}catch (MQException mqEx) {
						log.warn("\nMQSeries Error while disconnecting : Completion code " +mqEx.completionCode + " Reason code " + mqEx.reasonCode + " Cause: " + mqEx.getCause());
			}catch (Exception e) {
						log.warn("\nGeneral Error while disconnecting.\n " + e.toString());
			}
		}
		
}

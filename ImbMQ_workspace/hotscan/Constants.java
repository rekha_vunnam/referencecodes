/*************************************************************************************************************
-- Name         :  ChqEmbargoFilterPut.java
-- Description  :  This java program defines methods to connect Hotscan MQ server and put messages.
-- Author       :  Bala
-- Date 	    :  08/09/2008
--
-- Date		   Modidified By	SIR No		    Description
-- 08/09/2008   Bala            CHQ2921         Initial Version
**************************************************************************************************************/
package hotscan;


public class Constants {
	static public final String htscnQdtls="HTSCNQDTLS";
	static public final String sentSuccess="P";
	static public final String sentFailure="E";
	static public final String sendQ="send";
	static public final String rcvQ="rcv";
	static public final String noDataFound="NO DATA FOUND";
	
}

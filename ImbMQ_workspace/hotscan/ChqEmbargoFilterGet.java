/*************************************************************************************************************
-- Name         :  ChqEmbargoFilterGet.java
-- Description  :  This java program defines methods to connect Hotscan MQ server and put messages.
-- Author       :  Bala
-- Date 	    :  11/09/2008
--
-- Date		   Modidified By	SIR No		    Description
-- 11/09/2008   Bala            CHQ2921         Initial Version
-- 17/09/2012	Siva			AP71dbcq#1696	JDBC driver change due to 11g migration
**************************************************************************************************************/

package hotscan;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.List;

//Changed jdbc driver by AP71dbcq#1696
//import oracle.jdbc.driver.OracleCallableStatement;
import oracle.jdbc.OracleCallableStatement;
//import oracle.jdbc.driver.OracleTypes;
import oracle.jdbc.OracleTypes;

import org.apache.commons.logging.Log;

import com.ibm.mq.MQQueue;

public class ChqEmbargoFilterGet {

	private static Log log = null;	
	
	// Argument 1 = Database Username
	// Argument 2 = Database Encrypted Password
	// Argument 3 = Oracle SID
	// Argument 4 = Branch code
	// Argument 5 = Log Directory
	// Argument 6 = Hostname
	// Argument 7 = Oracle Port
	public static void main(String[] argv) {
	if(argv.length==7) {
      ChqEmbargoFilterGet obj_PutObj = new ChqEmbargoFilterGet();
      log = Util.initializeLogFile(argv[3],"ChqEmbargoFilterGet.class");
      String getReturnVal = obj_PutObj.mqHotscanGet(argv[0],argv[1],argv[2],argv[3],argv[4], argv[5], argv[6]);
      System.out.println(getReturnVal);
	} else {
		System.out.println("Insufficient number of arguments");
	}
}
	
/* The wrapper program for online put calls */
public String mqHotscanGet(String dbuserID, String dbpasswd, String dbSid, String branchCode, String logDir, String unixHostname, String dbPort) {
		try {
			String str_QueueDtlsIORet = new String();
			Connection conn = Database.setupConn(dbuserID,dbpasswd,dbSid, unixHostname, dbPort,branchCode);
			String[] str_QueueDtls = Database.getQueueDetailsFromDB(conn,Constants.htscnQdtls,branchCode,str_QueueDtlsIORet);
			MQInterface.initializeQueueManager(str_QueueDtls,branchCode);
			MQQueue queue = MQInterface.initializeQueue(str_QueueDtls[2],Constants.rcvQ);
			List strReceivedTrnID = MQInterface.getMessagesFromQueue(queue);
			MQInterface.disconnectFromQueue(queue);
			MQInterface.disconnectQueueManager();
			if(strReceivedTrnID.size()==0) {
				log.warn("\nNo messages available to receive from Hotscan");
			} else {			
				this.writeMessageToDatabase(conn, branchCode,logDir,strReceivedTrnID);
			}
			conn.close();
			return "0";
		} catch(Exception e) {
			return("-1");
		}
	}

/*Update the processed messages*/
  public void writeMessageToDatabase(Connection conn, String branchCode, String strLogDir,List strReceivedTrnID) {
			try {
				String ioRetStatus = new String();
				int rcvdCount = strReceivedTrnID.size();
				String[] updRcvdTrnID = new String[rcvdCount];
				int i=0;
				Iterator  it = strReceivedTrnID.listIterator();
				while(it.hasNext()) {
					updRcvdTrnID[i] = (String)it.next();
					i++;
					}
				OracleCallableStatement oracallbleStmt = (OracleCallableStatement)conn.prepareCall("begin cqk_p_hotscan_interface.cqp_rcv_from_hotscan(?,?); end;");
				oracallbleStmt.registerOutParameter(1,Types.VARCHAR);
				oracallbleStmt.setString(1,ioRetStatus);
				oracallbleStmt.setPlsqlIndexTable(2, updRcvdTrnID, rcvdCount, rcvdCount, OracleTypes.VARCHAR, 1000);
				oracallbleStmt.execute();
				ioRetStatus=(String)oracallbleStmt.getString(1);
				if (!ioRetStatus.equals("0")) {
					throw new SQLException();
				}
			}catch(Exception e) {
				log.warn("\nError while writing the Hotscan received messages into the database...\n"+e.toString());
			}
}





}
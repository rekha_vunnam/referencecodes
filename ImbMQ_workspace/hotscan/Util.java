/*************************************************************************************************************
-- Name         :  ChqEmbargoFilterPut.java
-- Description  :  This java program defines methods to connect Hotscan MQ server and put messages.
-- Author       :  Bala
-- Date 	    :  09/09/2008
--
-- Date		   Modidified By	SIR No		    Description
-- 09/09/2008   Bala            CHQ2921         Initial Version
**************************************************************************************************************/
package hotscan;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.Loader;

public class Util {

    /**
     * getCurrentTime - Generic Method to Get Current Time
     * @return String java.lang.String
     */
    public static String getCurrentTime() {
        return (new SimpleDateFormat("MM/dd/yyyy-kk:mm:ss").format(Calendar.getInstance().getTime()).toString());
    }

    /**
     * initializeLogFile - Method to initialize the log file
     * @param str_CompleteLogFilePath java.lang.String
     * @return String java.lang.String
     */
    public static Log initializeLogFile(String branchCode,String className)  {
    	Log log = null;
        try {
			URL url = Loader.getResource( branchCode + "DBCHQlog4j.properties");
			System.out.println("Branch Code: "+branchCode);
			System.out.println("Class Name: "+className);
			PropertyConfigurator.configure(url);
			log = LogFactory.getLog(className);				
			log.info("********START DATE MM/DD/YY-HH:MM:SS : ");
			log.info(Util.getCurrentTime()+" ********");
        } catch(Exception e) {
			log.warn("Java error while initialize the log file properties...\n"+e.toString());
        }     
        return(log);
    }
}

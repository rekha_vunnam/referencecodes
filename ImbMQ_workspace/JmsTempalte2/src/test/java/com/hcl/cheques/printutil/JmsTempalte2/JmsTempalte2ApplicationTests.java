package com.hcl.cheques.printutil.JmsTempalte2;

import java.util.Date;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.hcl.cheques.printutil.JmsTempalte2.config.IbmMqMessagePublisher;

@RunWith(SpringRunner.class)
class JmsTempalte2ApplicationTests {
	//private static final String destination= "example";
	//private static final String msg= "message";
	
	@InjectMocks
	private MessagePublisher publisher;
	
	@MockBean
	JmsTemplate jmsTemplate;
	
	/*@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
	    ReflectionTestUtils.setField(publisher, "destination", DESTINATION);
	}
	*/
	@Test
	void contextLoads() {
		publisher.publishMessage(new Message(1001L, "test body", new Date(),"queue1"));
	    verify(publisher.convertAndSend(DESTINATION, MESSAGE);
	}

	
	

}

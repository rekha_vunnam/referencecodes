package com.hcl.cheques.printutil.JmsTempalte2.config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.hcl.cheques.printutil.JmsTempalte2.main.JmsTempalte2Application;
import com.hcl.cheques.printutil.JmsTempalte2.model.Message;

@Component
public class IbmMqMessagePublisher implements MessagePublisher {
	@Autowired
	JmsTemplate jmsTemplate;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JmsTempalte2Application.class);
	    
	 @Override
	public void publishMessage(Message msg, String queue) {
		 jmsTemplate.convertAndSend(queue,msg);
		 LOGGER.info("sending a message");
	 }
	 
	 @JmsListener(destination = "jms.message.endpoint")
	    public void receiveMessage(Message msg) 
	    {
	        LOGGER.info("Received " + msg );
	    }

	
		
	 
}
		 


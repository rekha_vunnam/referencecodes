package com.hcl.cheques.printutil.JmsTempalte2.config;

import org.springframework.jms.core.JmsTemplate;

import com.hcl.cheques.printutil.JmsTempalte2.model.Message;

public interface MessagePublisher {
	
	//public void publishMessage(JmsTemplate jmsTemplate,Message msg, String queueName);

	public void publishMessage(Message msg, String queue);

}

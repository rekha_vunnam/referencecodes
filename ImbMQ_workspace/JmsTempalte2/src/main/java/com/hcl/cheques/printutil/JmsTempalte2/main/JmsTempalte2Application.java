package com.hcl.cheques.printutil.JmsTempalte2.main;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import com.hcl.cheques.printutil.JmsTempalte2.config.MessagePublisher;
import com.hcl.cheques.printutil.JmsTempalte2.model.Message;

@SpringBootApplication
c
public class JmsTempalte2Application implements CommandLineRunner {
	@Autowired
	MessagePublisher publisher;
	public static void main(String[] args) {
		 SpringApplication.run(JmsTempalte2Application.class, args);
		/*ConfigurableApplicationContext context = SpringApplication.run(JmsApplication.class, args);
        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);*/
        //System.out.println("Sending a message.");
       
    }

	@Override
	public void run(String... args) throws Exception {
		
	        publisher.publishMessage(new Message(1001L, "test body", new Date()), "jms.message.endpoint");	
	}
	}


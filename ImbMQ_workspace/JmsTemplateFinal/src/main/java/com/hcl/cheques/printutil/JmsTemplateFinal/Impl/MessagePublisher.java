package com.hcl.cheques.printutil.JmsTemplateFinal.Impl;


import com.hcl.cheques.printutil.JmsTemplateFinal.model.Message;



public interface MessagePublisher {
	
	//public void publishMessage(JmsTemplate jmsTemplate,Message msg, String queueName);

	public void publishMessage(Message msg, String queue);

}

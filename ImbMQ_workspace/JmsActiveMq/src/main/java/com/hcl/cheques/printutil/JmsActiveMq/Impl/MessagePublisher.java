package com.hcl.cheques.printutil.JmsActiveMq.Impl;

import com.hcl.cheques.printutil.JmsActiveMq.model.Message;

public interface MessagePublisher {
	
	public void publishMessage(Message msg, String queue);

}

package com.hcl.cheques.printutil.JmsActiveMq;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import com.hcl.cheques.printutil.JmsActiveMq.Impl.MessagePublisher;
import com.hcl.cheques.printutil.JmsActiveMq.model.Message;


@EnableJms
@SpringBootApplication
public class JmsActiveMqApplication implements CommandLineRunner {
     
	@Autowired
	MessagePublisher publisher;
	
	public static void main(String[] args) {

		SpringApplication.run(JmsActiveMqApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		 publisher.publishMessage(new Message(1001L, "test body", new Date()), "jms.message.endpoint");	
			

	}

}

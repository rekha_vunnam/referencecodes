package com.hcl.cheques.printutil.JmsActiveMq;

import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.cheques.printutil.JmsActiveMq.Impl.MessagePublisher;
import com.hcl.cheques.printutil.JmsActiveMq.model.Message;



@RunWith(MockitoJUnitRunner.class)
public class JmsActiveMqApplicationTests {

	@Mock
	private MessagePublisher messagePublisher;

	Message message;
	String queueName;

	@Before
	public void init() {
		message = new Message(1001L, "test body", new Date());
		queueName = "jms.message.endpoint";
	}

	@Test
	public void testPublishMessage() {
		messagePublisher.publishMessage(message, queueName);
		verify(messagePublisher).publishMessage(message, queueName);
	}

}
